// Utility to build a pangenome from a blastp against a taxids` extracted genes

package pangenome

import(
    "bufio"
    "fmt"
    "os"
    "strconv"
    "strings"

    "bitbucket.org/GottySG36/node"
)

type Counts []int

func (c Counts) Merge(m Counts) (Counts, error) {
    if len(c) != len(m) {
        err := fmt.Errorf("Cannot merge with two slice of different dimensions (%v vs %v)", len(c), len(m))
        return c, err
    }
    for i, n := range m {
        c[i] += n
    }
    return c, nil
}

type Gene struct {
    Name        string
    Category    string
    Counts      Counts
}

type Pangenome map[string]*Gene

type Taxids map[string]int

type txt []string
func (t txt) Csv() string {
    var sep string
    var b strings.Builder
    for _, val := range t {
        b.WriteString(sep)
        b.WriteString(val)
        sep = ","
    }
    return b.String()
}

func (c Counts) Csv() string {
    var sep string
    var b strings.Builder
    for _, ct := range c {
        b.WriteString(sep)
        b.WriteString(strconv.Itoa(ct))
        sep = ","
    }
    return b.String()
}

func LoadPangenome(f string) (Pangenome, Taxids, error) {
    pang := make(Pangenome)
    tax := make(Taxids)
    i, err := os.Open(f)
    defer i.Close()
    if err != nil {
        return pang, tax, err
    }

    s := bufio.NewScanner(i)
    s.Scan()
    header := s.Text()
    ids := strings.Split(header, ",")[2:]
    for i, t := range ids {
        tax[t] = i
    }

    for s.Scan() {
        line := strings.Split(s.Text(), ",")
        g := new(Gene)
        g.Name = line[0]
        g.Category = line[1]
        g.Counts = make(Counts, len(tax))
        for i, n := range line[2:] {
            if s, err := strconv.Atoi(n); err == nil {
                g.Counts[i] = s
            } else {
                return pang, tax, err
            }
        }
        pang[g.Name] = g
    }
    return pang, tax, nil
}

type Converter map[string]string

func ConversionTable(s [][]*node.Node) Converter {
    conv := make(Converter)
    for id, cluster := range s {
        for _, gene := range cluster {
            conv[gene.Name] = fmt.Sprintf("GeneCluster%v_%v", id, len(cluster))
        }
    }
    return conv
}

type Clusters map[string][]string

func (c Converter) GetClusters() Clusters {
    cl := make(Clusters)
    for k, v := range c {
        cl[v] = append(cl[v], k)
    }
    return cl
}

func (c Clusters) Write(o string) error {
    f, err := os.Create(fmt.Sprintf("%v/cluster-list.txt", o))
    defer f.Close()
    if err != nil {
        return err
    }

    w := bufio.NewWriter(f)
    for name, list := range c {
        _, err := w.WriteString(name+"\n")
        if err != nil {
            return err
        }
        for idx, seq := range list {
            _, err := w.WriteString(fmt.Sprintf("%v : %v\n", idx, seq))
            if err != nil {
                return err
            }
        }
    }
    w.Flush()
    return nil
}

func (p Pangenome) Cluster(c Converter, core, acc float64) (Pangenome, error) {
    pangCl := make(Pangenome)
    for _, g := range p {
        if _, ok := pangCl[c[g.Name]]; !ok {
            pangCl[c[g.Name]] = &Gene{Name:c[g.Name],
                                         Category:g.Category,
                                         Counts:g.Counts}
        } else {
            mergeC, err := pangCl[c[g.Name]].Counts.Merge(g.Counts)
            if err != nil {
                return pangCl, err
            }
            pangCl[c[g.Name]].Counts = mergeC
        }

        pangCl[c[g.Name]].Category = pangCl[c[g.Name]].Counts.Categorize(core, acc)
    }
    return pangCl, nil
}

func (p Pangenome) Write(o string, t Taxids) error {
    f, err := os.OpenFile(fmt.Sprintf("%v/pangenome.csv", o), os.O_CREATE|os.O_WRONLY, 0644)
    defer f.Close()
    if err != nil {
        return err
    }

    w := bufio.NewWriter(f)

    header := make(txt, len(t))
    for k, idx := range t {
        header[idx] = k
    }

    fmt.Fprintf(w, "Genes,Category,%v\n", header.Csv())

    for key, val := range p {
        fmt.Fprintf(w, "%v,%v,%v\n", key, val.Category, val.Counts.Csv())
    }

    w.Flush()

    return nil
}

func (p Pangenome) CoreGenes() map[string]bool {
    core := make(map[string]bool, 0)
    for _, gene := range p {
        if gene.Category == "Core" {
            core[gene.Name] = true
        }
    }
    return core
}

func (p Pangenome) AccGenes() map[string]bool {
    acc := make(map[string]bool, 0)
    for _, gene := range p {
        if gene.Category == "Accessory" {
            acc[gene.Name] = true
        }
    }
    return acc
}

func (p Pangenome) SpecGenes() map[string]bool {
    spec := make(map[string]bool, 0)
    for _, gene := range p {
        if gene.Category == "Specific" {
            spec[gene.Name] = true
        }
    }
    return spec
}

func (p Pangenome) Pretty(nb int, t Taxids) {
    header := make(txt, len(t))
    for k, idx := range t {
        header[idx] = k
    }
    if (nb <= 0) || (nb > len(p)) {
        nb = len(p)
    }
    fmt.Printf("Genes\tCategory\t%v\n", header.Csv())
    var counter int
    for key, val := range p {
        if counter >= nb {
            break
        } else {
            fmt.Printf("%v\t%v\t%v\n", key, val.Category, val.Counts.Csv())
            counter++
        }
    }
}

func (c Counts) Categorize(core, acc float64) string {
    var counter float64
    if 100.0 * 1 / float64(len(c)) > acc {
        acc = 100.0 * 1 / float64(len(c))
    }
    for _, nb := range c {
        if nb > 0 {
            counter++
        }
    }
    perc := 100.0 * counter / float64(len(c))
    if perc >= core {
        return "Core"
    } else if  perc < core && perc > acc {
        return "Accessory"
    } else {
        return "Specific"
    }
}

type Summary struct {
    Core        int
    Accessory   int
    Specific    int
}

func (p Pangenome) Summarize() Summary {
    s := Summary{}
    for _, g := range p {
        switch cat := g.Category; cat {
        case "Core":
            s.Core++
        case "Accessory":
            s.Accessory++
        case "Specific":
            s.Specific++
        }
    }
    return s
}

func (s Summary) Write(o string) error {
    f, err := os.OpenFile(fmt.Sprintf("%v/summary.txt", o), os.O_CREATE|os.O_WRONLY, 0644)
    defer f.Close()
    if err != nil {
        return err
    }
    w := bufio.NewWriter(f)
    fmt.Fprintf(w, "Summary information\nCore : %v\nAccessory : %v\nSpecific : %v\n",
               s.Core, s.Accessory, s.Specific)
    w.Flush()

    return nil
}

func (s Summary) Print() {
    fmt.Printf("Summary information\nCore : %v\nAccessory : %v\nSpecific : %v\n",
               s.Core, s.Accessory, s.Specific)
}
